#include <iostream>

#ifdef _DEBUG 
#define VERBOSE_LOG(msg) \
    std::cout << __FILE__ << ":" << __func__ << ":" << __LINE__ << " " << msg << std::endl
#else
#define VERBOSE_LOG(msg)
#endif

int main()
{
	VERBOSE_LOG("Hello");
	return 0;
}
